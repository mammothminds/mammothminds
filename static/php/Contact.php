<?php

// require_once 'PHPMailer/PHPMailerAutoload.php';

header('Content-Type: application/json');

$isVerified = false;

// if ($_SERVER["REQUEST_METHOD"] == "POST") {
// 	$name = trim($_POST['full_name']);
// 	$emailAddress = trim($_POST['email_address']);
// 	$contactNumber = trim($_POST['mobile_number']);
// 	$message = trim($_POST['message']);

// 	$email_body = "";

// 	if (isset($name) && isset($emailAddress) && isset($contactNumber)) {
// 		$isVerified = true;
// 	}

// 	if ($isVerified == true) {
// 		$mail = new PHPMailer;

// 		$mail->isSMTP();
// 		// $mail->Host = 'smtp.gmail.com';
// 		$mail->SMTPDebug = 2;
// 		// $mail->SMTPAuth = true;
// 		$mail->Username = 'ppcrv.champ@gmail.com';
// 		$mail->Password = 'PPCRVch@mp';
// 		// $mail->SMTPSecure = 'ssl';
// 		// $mail->Port = 465;

// 		// $mail->SMTPSecure = 'tls';
// 		// $mail->Port = 587;

// 		$mail->Host = 'relay-hosting.secureserver.net';
// 		$mail->Port = 25;
// 		$mail->SMTPAuth = false;
// 		$mail->SMTPSecure = false;

// 		$email_body = "<br><br><br>";
// 		$email_body = $email_body . 'Name: <strong>' . $name . "</strong><br>";
// 		$email_body = $email_body . 'Email Address: <strong>' . $emailAddress . "</strong><br>";
// 		$email_body = $email_body . 'Contact Number: <strong>' . $contactNumber . "</strong><br>";
// 		$email_body = $email_body . 'Message: <strong>' . $message . "</strong><br>";

// 		// Set sender information
// 		$mail->setFrom($emailAddress, $name);

// 		// Set email to Sent to
// 		$mail->addAddress('ajbenederio@gmail.com');
// 		// $mail->addAddress('ppcrv.champ@gmail.com');

// 		// Add email In Reply To
// 		$mail->addReplyTo($emailAddress);

// 		// Add email to CC
// 		// $mail->addCC('alliah@autodeal.com.ph');

// 		$mail->isHTML(true);

// 		$mail->Subject = $name . ' wants to contact you!';
// 		$mail->Body = $email_body;

// 		if($mail->send()) {
// 			http_response_code(200);

// 		    echo json_encode(array(
// 		    	'message' => 'success',
// 		    	'responseText' => "Message has been sent successfully.",
// 		    	// 'status' => 200
// 		    ));
// 		} else {
// 			http_response_code(500);

// 		    $responseText =  'Message could not be sent.';
// 		    $responseText .= 'Mailer Error: ' . $mail->ErrorInfo;

// 		    echo json_encode(array(
// 		    	'message' => 'error',
// 		    	'responseText' => $responseText,
// 		    	// 'status' => 400
// 		    ));

// 		    // echo "Message could not be sent.";
// 		    // echo "Mailer Error: " . $mail->ErrorInfo;
// 		    // exit;
// 		}
// 	} else {
// 		http_response_code(403);

// 		echo json_encode(array(
// 	    	'message' => 'error',
// 	    	'responseText' => 'Required fields empty.',
// 	    	// 'status' => 400
// 	    ));
// 	} 
	
// }


if ($_SERVER["REQUEST_METHOD"] == "POST") {

	$name = trim($_POST['full_name']);
	$emailAddress = trim($_POST['email_address']);
	$contactNumber = trim($_POST['mobile_number']);
	$message = trim($_POST['message']);

	$email_body = "";

	if (isset($name) && isset($emailAddress) && isset($contactNumber)) {
		$isVerified = true;
	}

	if ($isVerified == true) {
		$to = "hello@mammothminds.co"; // <– replace with your address here

		$subject = $name . " wants to contact you.";
		// $message = "Hello! This is a simple test email message.";
		$from = $emailAddress;
		$headers = "From:" . $from;

		$email_body = "\n\n\n";
		$email_body = $email_body . 'Name: ' . $name . "\n";
		$email_body = $email_body . 'Email Address: ' . $emailAddress . "\n";
		$email_body = $email_body . 'Contact Number: ' . $contactNumber . "\n";
		$email_body = $email_body . 'Message: ' . $message . "\n";

		$message = $email_body;
		
		$result = mail($to,$subject,$message,$headers);

		if (!$result) {
			http_response_code(500);

		    $responseText =  'Message could not be sent.';
		    $responseText .= 'Mailer Error: ' . $mail->ErrorInfo;

		    echo json_encode(array(
		    	'message' => 'error',
		    	'responseText' => $responseText,
		    	// 'status' => 400
		    ));
		} else {
			http_response_code(200);

		    echo json_encode(array(
		    	'message' => 'success',
		    	'responseText' => "Message has been sent successfully.",
		    	// 'status' => 200
		    ));
		}
	} else {
		http_response_code(403);

		echo json_encode(array(
	    	'message' => 'error',
	    	'responseText' => 'Required fields empty.',
	    	// 'status' => 400
	    ));
	} 
}