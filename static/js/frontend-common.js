$(document).ready(function(){
	//material contact form animation
	$('.contact-form').find('.form-control').each(function() {
	  var targetItem = $(this).parent();
	  if ($(this).val()) {
	    $(targetItem).find('label').css({
	      'top': '10px',
	      'fontSize': '14px'
	    });
	  }
	})
	$('.contact-form').find('.form-control').focus(function() {
	  $(this).parent('.input-block').addClass('focus');
	  $(this).parent().find('label').animate({
	    'top': '10px',
	    'fontSize': '14px'
	  }, 300);
	})
	$('.contact-form').find('.form-control').blur(function() {
	  if ($(this).val().length == 0) {
	    $(this).parent('.input-block').removeClass('focus');
	    $(this).parent().find('label').animate({
	      'top': '28px',
	      'fontSize': '14px'
	    }, 300);
	  }
	});	
});

(function($){
// Modal
    $.fn.modal = function(options){
        var settings = $.extend({
            'callback': false,
            'before_show': false,
            'close_on_unfocus': false,
            'no_overflow': false
        }, options);

        var modalId = $(this).data('modal-id');
        var modal = $('#'+modalId);
        var close = $('.close, .exit, .closeModal', modal);
        var body = $('body');

        $(this).each(function(){
            $(this).on('click', function(e){
                var This = $(this);
                modalId = $(this).data('modal-id');
                modal = $('#'+modalId);
                close = $('.close, .exit, .closeModal', modal);

                if($.isFunction(settings.before_show)){
                    settings.before_show(options, This, modalId);
                }

                e.preventDefault();
                modal.show();

                if (!settings.no_overflow && modal.css('display') == 'block') {
                    // body.css('overflow','hidden');
                    body.addClass('modalopen');
                }

                else {
                    scrollBody();
                }

                if($.isFunction(settings.callback)){
                    settings.callback(options, This, modalId);
                }

            });
        });

        if (settings.close_on_unfocus) {
            var trigger = $(this);

            // Clicking outside of dropdown will close target
            $(document).click(function(e){
                var parentAnchor = $(e.target).closest('a');
                if( !parentAnchor.is(trigger) && modal.has(e.target).length === 0) {
                    modal.hide();
                }
            });
        }

        close.on('click', function(e){
            e.preventDefault();
            modal.hide();
            scrollBody();
        });

        var scrollBody = function() {
            body.removeClass('modal-open');
            return;
        };

        return this;
    };
})(jQuery);