$(document).ready(function(){
	$('form#contactForm #full_name').focusout(function(){
		if ($(this).val().length === 0) {
			$('p.fn-error').removeClass('d-hidden');
		} else {
			$('p.fn-error').addClass('d-hidden');
		}
	});

	$('form#contactForm #email_address').focusout(function(){
		if ($(this).val().length === 0) {
			$('p.ea-error').removeClass('d-hidden');
		} else {
			$('p.ea-error').addClass('d-hidden');
		}
	});

	$('form#contactForm #mobile_number').focusout(function(){
		if ($(this).val().length === 0) {
			$('p.mn-error').removeClass('d-hidden');
		} else {
			$('p.mn-error').addClass('d-hidden');
		}
	});

	$('form#contactForm #message').focusout(function(){
		if ($(this).val().length === 0) {
			$('p.msg-error').removeClass('d-hidden');
		} else {
			$('p.msg-error').addClass('d-hidden');
		}
	});

	$('form#contactForm').on('submit', function(e){
		e.preventDefault();

		var data = $(this).serialize();
		var isValid = validateForm();

		if (isValid == true) {
			$('input.submitButton').prop('value', 'Processing...').prop('disabled', 'disabled');

			// var path = "../php/Contact.php";

			$.ajax({
				type: "POST",
				url: path,
				data: data,
				success: function(json){
					$('body').addClass('modalopen');

					// if ($('form#contactForm').hasClass('formModal')) {
						
					// }

					var modalId = $('input.submitButton').data('modal-id');
					var modal = $('#'+modalId);
					modal.removeClass('d-hidden').css('display', 'block');

					resetAllFields();

					$('.submitButton').prop('value', 'Send Message').removeProp('disabled');
				},
				error: function(json){
					$('input.submitButton').prop('value', 'Send Message').removeProp('disabled');
				}
			});
		}

		return false;
	});

	// $('.launch-modal').modal();

	function validateForm()
	{
		var isValid = true;
		if ($('form#contactForm #full_name').val().length === 0) {
			isValid = false;
			$('p.fn-error').removeClass('d-hidden');
		} else {
			$('p.fn-error').addClass('d-hidden');
		}

		// var emailRegex = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i
		if ($('form#contactForm #email_address').val().length === 0) {
			isValid = false;
			$('p.ea-error').removeClass('d-hidden');
		} else {
			var isEmailValid = validateEmail($('form#contactForm #email_address').val());
			if (isEmailValid == true) {
				$('p.ea-error').addClass('d-hidden');
			} else {
				isValid = false;
			}
		}

		if ($('form#contactForm #mobile_number').val().length === 0) {
			isValid = false;
			$('p.mn-error').removeClass('d-hidden');
		} else {
			$('p.mn-error').addClass('d-hidden');
		}

		if ($('form#contactForm #message').val().length === 0) {
			isValid = false;
			$('p.msg-error').removeClass('d-hidden');
		} else {
			$('p.msg-error').addClass('d-hidden');
		}

				return isValid;
			}

			function validateEmail(email) {
			    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/i;
			    if( !emailReg.test( email ) ) {
			        return false;
			    } else {
			        return true;
			    }
			}

			function resetAllFields()
			{
				$('form#contactForm').find(":input[type=text]").val('');
				$('form#contactForm').find(":input[type=email]").val('');
				$('form#contactForm').find('textarea').val('');
			}	
		});